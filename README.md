# mpv-discord-rich-presence

Lua script and helper Python script for the `mpv` player which adds Rich Presence integration with Discord when listening to music. **Currently prototype quality, only tested on Linux. Will not work on Windows.**

# Dependencies
Python 3

# Instructions

Place the `discord_rpc` and `scripts` folders in `~/.config/mpv`. mpv should now show up on your Discord status when music is being played.

# License

This repository is distributed under the GNU GPL v2. See LICENSE for further information.

`rpc.py` is distributed under the MIT License: 

MIT License

Copyright (c) 2017 Snazzah
Copyright (c) 2017-2018 FichteFoll <fichtefoll2@googlemail.com>
Copyright (c) 2018 Anson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.